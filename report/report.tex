% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}

\usepackage{subcaption}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}


% Set TITLE, AUTHOR and DATE
\title{A report of the implementation of the rules of Go in C++}
\author{Andreas Dyr{\o}y Jansson}
\date{\today}



\begin{document}



  % Create the main title section
  \maketitle

  \begin{abstract}
    Currently, the largest game of Go solved by an artificial intelligence was played on a 5 by 5 board.
    This article describes the process and results of implementing the rules of the ancient board game Go in C++
    in order to make an AI able to play the game.
    The solution was based on a simple template application containing a graphical client, a game engine, and
    a basic random artificial intelligence.
  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \section{Introduction}
  The game of Go is an ancient board game originating from China. Two players play the game using black and white stones,
  placed on a board made up of a grid of intersecting lines. Due to the simplistic nature of the rules, the number of
  possible moves is practically endless, unlike for instance chess, which has a strict set of rules. Therefore, no computer
  program has yet been able to solve Go for larger boards than 5 by 5\cite{solvingGo:2003}. This report discusses the
  process and results of trying to solve Go by implementing the rules and creating an AI in a
  multithreaded C++ application.


  \subsection{The rules of Go}
  The rules of Go are as mentioned rather simple, and therefore allow a vast number of different moves. The game is
  normally played on a board made up of 19 times 19 intersections, but may be scaled for any board size. The black player
  has the opening move. The main goal of the game is to capture area, by placing stones in a strategic way to block the opponent
  from placing their stones. A stone may not be placed on top of another. Adjacent stones of the same color is referred to as
  blocks. Blocks may have one or more boundary, which are points inside the board surrounding the block (\cref{fig:bound}). points one the boundary
  not occupied by other stone is a so-called liberty. Players can capture blocks
  by surrounding an opponent’s block, occupying all liberties. The captured stones are removed from the board.
  One important rule of Go is the no-suicide rule (\cref{fig:suicide}). It is not allowed to place a stone if the spot is already surrounded by
  enemy stones, or if placing the stone occupies the last freedom of a block of the same color. However, suicide is allowed if
  the move is able to capture other stones, leaving one or more liberties for the placed stone (\cref{fig:capture}).
  Capturing and removing stones from the board opens up the possibility of repeating the same moves an infinite number of times.
  To avoid such infinite loops, the KO-rule states that playing the same move twice in a row is forbidden. To account for longer
  repeating cycles, there is the super-KO rule. The super-KO rule prevents players from repeating the same cycle over the course
  of a game.\newline
  The game may end in different ways. Players may pass their turn, and if both players pass the game ends. Players may otherwise
  agree to end the game, due to repeating moves or a stalemate. The two main methods of calculating score are the Chinese rule
  and the Japanese rule The Chinese rule counts the surrounded territory and stones placed on the board, while the Japanese
  rule counts captured stones and surrounded area (from section 2 - Rules of the game\cite{solvingGo:2003}).
  \begin{figure}[tbp]
  \centering
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[width=\linewidth]{gfx/boundaries.jpg}
    \caption{Boundaries for two adjacent blocks}
    \label{fig:bound}
  \end{subfigure}
  \quad
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[width=\linewidth]{gfx/white_suicide.jpg}
    \caption{This move is suicide for White}
    \label{fig:suicide}
  \end{subfigure}
  \quad
  \begin{subfigure}{0.3\textwidth}
    \centering
    \includegraphics[width=\linewidth]{gfx/capture.jpg}
    \caption{White may place here to capture the Black block and avoid suicide}
    \label{fig:capture}
  \end{subfigure}
  \caption{Various concepts of Go}
  \label{fig:concepts}
  \end{figure}

  \section{Implementation}
  To make the AI able to play the game, the rules had to be implemented in a way the AI could understand. In the given template
  application, the AI asks the board for valid positions, and the board performs a series of checks to verify if the move asked
  is valid. If the position is valid, the AI can play out the move. The same functionality is used for human players, making
  the game easier for inexperienced players. The game keeps track of legal moves, captures and score.\newline

In order to implement the basic rules of Go, appropriate containers from the Standard Template Library were utilized. The stones
on the board were stored as key- and value-pairs in a map, the key being the position on the board, and the value being the stone
itself. Blocks are a member of the Board, contained in a List. The Block consists of stones, stored in a List.  Each Block may have
one or more Boundaries, also stored in a List. Each Boundary consists of a Vector of points. Using a map for storing stones is a
good way of combining objects with positions. The map of points and stones are used to render the current board in the client.
A list has a linear time for removing, inserting and searching\cite{cpplist:2015}, and was therefore chosen to store the majority of the objects,
which are frequently removed, inserted or searched through during a game of Go. The points of the boundaries need to be ordered;
therefore, a Vector was chosen to do the job.\newline

Every time a player wants to place a stone, the board checks if the desired position is legal according to the rules. In order to
check if an intersection is available, the board first checks if the given point is inside the board. If the point is inside the
board, it is checked if another stone already occupies the spot. If the spot is available, the board checks for suicide. If the
move leads to suicide, the stone may still be placed if it manages to capture other stones in the process. If no stone can be captured,
the move is illegal. In order to check for suicide, the board asks if any surrounding blocks of the same color exists. If so, the
board asks the block how many freedoms it has. If the block has only has one liberty, the move will result in suicide. If the block
has more than one, it is not possible to cause suicide by placing one stone, and the move is always legal. The same check is
performed to see if a block can be captured. If a block of the opposite color exists with only one freedom, the block will have
no liberties after the move, and as a result be captured.\newline

After the move is validated as legal according to the rules, the stone may be placed. A new block with boundaries is created every
time a stone is placed. The board then checks every adjacent intersection for blocks of the same color. If one exists, the blocks
will join to form one larger block. In theory, placing one stone may end up joining up to four different blocks. Therefore, the
placed stone is first added to the block above it, then that block is added to the one to the left, and so on, until all directions
are checked.\newline

When a block is created, each surrounding point inside the board is added to the boundary. Blocks are joined by first copying all
points from one to the other, and then joining the boundaries. Boundaries are joined by keeping track of the point of the inserted
stone (joiningPoint), and the point in the block intersecting the boundary of the stone (pointInBlock). All the points are then copied,
and the two points occupied by the stones are removed.\newline

Polar sorting was used as a way to order points in the boundaries. Polar sort is a way of sorting points in a 2D plane by calculating
the angle of the points to an average point, similar to the sorting step of the Graham Scan\cite{graham:1972}. The interior point is
calculated by summing the x- and y-values and dividing
by the number of points. The angle is calculated using the atan2 of the cmath library. By implementing a custom compare-function and
passing it to the sort-function in STL, it is possible to sort the surrounding points of a block either clockwise or counter clockwise.
However, the order of the elements does not matter in the current implementation of the basic rules.\newline

After the points were sorted, the uniqe-function of STL was used to remove any duplicate points from the boundary.\newline

Searching through boundaries and blocks was achieved by implementing the find-function of STL. Find uses iterators to search a
container for a given value. Every time a player places a stone, all surrounding intersections were checked for blocks. If a
block of the same color is found, the placed stone is added to the block. A stone may be added to multiple adjacent blocks,
creating one, large block. In order to respect the suicide rule, every time a player tries to place a stone, the surrounding blocks
of the same color are checked for liberties. If one or more adjacent blocks only have one liberty, the move will result in suicide.
One the other hand, if a stone of opposite color tries the same move, the block will captured, earning points to the capturing player
and removing the stones from the map.\newline

Shared pointers were heavily used throughout the solution. Shared pointers allow the same data to be shared between multiple objects,
and passing pointers to functions instead of copying the object itself allows for more direct access to modify the object inside
other functions. Block contains a shared pointer of board, so the block can ask the board about stones, size and so on. Each stone also
contains a shared pointer to the board, in order to know about its immediate stones.\newline

When the game is over, the list and map containing blocks and points are cleared. Since most of the solution is based on shared
pointers, all objects no longer used will be deleted in a cascade-like way.\newline


\section{Results}
Searching through the containers were done using mostly for-loops combined with the find function of STL. According to cplusplus.com,
the complexity of find is up to linear in the distance between the first and the last iterator\cite{cppfind:2015}.\newline

In the current implementation, suicide is not allowed unless the move manages to capture other stones. Whenever a block is removed,
the number of stones captured is added to the opponent’s score. Counting the number of stones is the only implemented way
of calculating score.\newline

During the development, a total of 29 unit tests were created in order to test functionality.\newline

As a result of time constraints, no AI was implemented in the solution, beside the random AI provided by the template
application. There is also no option to pass the turn. This means that if no valid moved is detected by the AI, the game
will never end.\newline

The client application remained mostly unchanged, except for a different background image and a scoreboard. Captured stones are
also visually removed from the board (see \cref{fig:client}).\newline

The game is playable using the client, and players need not keep track of captures, legal positions and scoring themselves. All is
done by the implementation.\newline

\begin{figure}[tbp]
  \centering
  \includegraphics[width=0.80\textwidth]{gfx/client.jpg}
  \caption{Client displaying scores and captures}
  \label{fig:client}
\end{figure}


  \section{Concluding remarks}
  Implementing the basic rules of Go in C++ proved to be quite the challenge. In order to solve Go for 19x19 boards, the plan was
  to implemented a custom AI using different opening strategies, heuristic functions and the ability to pass its turn if there are no
  possible moves left. However, due to the time spent visualizing and programming the rules, no time was left at the end of the
  project to implement the AI.\newline

  The current way of counting liberties does depend on a block having multiple boundaries. However, support for blocks with
  multiple boundaries is included. Implementing a way to split boundaries that are divided by other stones or borders
  would in theory speed up the counting of freedoms, as fewer points would need be be checked.\newline

  Another missing rule is the option for players to skip their turn. As it currently stands, the players themselves have to agree
  on when to end a game. When two AI's play, if no legal position is found the game will lock up (\cref{fig:stalemate}). There is also no KO-rule, which checks for repeating cycles of moves. If an AI were to play the game in its
  current form, it may get itself stuck on endless cycles of the same move. Human players who know the rules of Go will not face this
  problem.\newline

\begin{figure}[tbp]
  \centering
  \includegraphics[width=0.80\textwidth]{gfx/stalemate.jpg}
  \caption{Black has no legal moves left, producing a stalemate}
  \label{fig:stalemate}
\end{figure}

  Multithreading was to be implemented as a way of making the AI think faster. On a 19 times 19 sized board, the number of different
  combinations of legal moves may be close to infinitely large. Using just a single thread to search through all the generated
  game trees would be most unpractical. Another use of multithreading in the current implementation would be making searches in
  blocks and boundaries faster.\newline

  Implementing all the missing rules of Go would be the first step of further development. After all KO-rules, proper scoring
  calculations, and the option to pass a turn were fully implemented and working, creating the AI would be the next step. The AI
  would need to be able to think ahead, using the rules to anticipate all legal moves and calculating the final score of each
  chain of moves.\newline

  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}

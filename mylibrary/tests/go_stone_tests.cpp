// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>
#include "../../mylibrary/stone.h"


namespace go = mylib::go;

TEST(Go_Stones_Test,TestHasNorth) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x x W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneNorth = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSouth = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    board->placeStone(stoneNorth);
    board->placeStone(stoneSouth);


    // EXPECT there to be a stone north of this
    EXPECT_TRUE(stoneSouth->hasNorth());

    // EXPECT there to be no other stones
    EXPECT_FALSE(stoneSouth->hasSouth());

    EXPECT_FALSE(stoneSouth->hasEast());

    EXPECT_FALSE(stoneSouth->hasWest());
}

TEST(Go_Stones_Test,TestHasSouth) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x x W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneNorth = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSouth = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    board->placeStone(stoneNorth);
    board->placeStone(stoneSouth);


    // EXPECT there to be a stone south of this
    EXPECT_TRUE(stoneNorth->hasSouth());

    // EXPECT there to be no other stones
    EXPECT_FALSE(stoneNorth->hasNorth());

    EXPECT_FALSE(stoneNorth->hasEast());

    EXPECT_FALSE(stoneNorth->hasWest());
}

TEST(Go_Stones_Test,TestHasEast) {
    //      |x x x x x|
    //      |x x x x x|
    //      |x x W W x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneWest = std::make_shared<go::Stone>(W, go::Point(3,3), board);
    auto stoneEast = std::make_shared<go::Stone>(W, go::Point(4,3), board);

    board->placeStone(stoneWest);
    board->placeStone(stoneEast);


    // EXPECT there to be a stone east of this
    EXPECT_TRUE(stoneWest->hasEast());

    // EXPECT there to be no other stones
    EXPECT_FALSE(stoneWest->hasNorth());

    EXPECT_FALSE(stoneWest->hasWest());

    EXPECT_FALSE(stoneWest->hasSouth());
}

TEST(Go_Stones_Test,TestHasWest) {
    //      |x x x x x|
    //      |x x x x x|
    //      |x W W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneWest = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneEast = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    board->placeStone(stoneWest);
    board->placeStone(stoneEast);


    // EXPECT there to be a stone east of this
    EXPECT_TRUE(stoneEast->hasWest());

    // EXPECT there to be no other stones
    EXPECT_FALSE(stoneEast->hasNorth());

    EXPECT_FALSE(stoneEast->hasEast());

    EXPECT_FALSE(stoneEast->hasSouth());
}

TEST(Go_Stones_Test,TestOutOfBounds) {
    //    W |x x x x x|
    //      |x x x x x|
    //      |x x x x x|
    //      |x x x x x|
    //      |x x x x x|


    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));


    // EXPECT there not to be a stone here
    EXPECT_FALSE(board->isNextPositionValid(go::Point(-2,1)));
}

TEST(Go_Stones_Test,TestOutOfBounds2) {
    //    W |x x x x x|
    //      |x x x x x|
    //      |x x x x x|
    //      |x x x x x|
    //      |x x x x x|


    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));


    // EXPECT there not to be a stone here
    EXPECT_FALSE(board->isNextPositionValid(go::Point(6,8)));
}

TEST(Go_Stones_Test,TestCreateBlock) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x x W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSecond = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    board->placeStone(stoneFirst);
    board->placeStone(stoneSecond);

    auto block = *board->blocks().begin();

    // EXPECT there should be 1 block on the board
    EXPECT_EQ(1, board->blocks().size());

    // EXPECT this block should contain 2 elements
    EXPECT_EQ(2, block->points().size());
}

TEST(Go_Stones_Test,TestJoinBlocks) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x W W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSecond = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneThird = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    auto block1 = std::make_shared<go::Block>(board);
    auto block2 = std::make_shared<go::Block>(board);
    auto block3 = std::make_shared<go::Block>(board);

    block1->addPoint(stoneFirst->point());
    block2->addPoint(stoneSecond->point());
    block3->addPoint(stoneThird->point());

    board->addBlock(block1);
    board->addBlock(block2);
    board->addBlock(block3);

    board->joinBlocks(block1, block2, stoneFirst->point(), stoneSecond->point());
    board->joinBlocks(block1, block3, stoneFirst->point(), stoneThird->point());

    // EXPECT there should be 1 block on the board
    EXPECT_EQ(1, board->blocks().size());

    // EXPECT this block should contain 3 elements
    EXPECT_EQ(3, block1->points().size());
}

TEST(Go_Stones_Test,TestFindBlock) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x W W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSecond = std::make_shared<go::Stone>(W, go::Point(2,3), board);

    auto block1 = std::make_shared<go::Block>(board);
    auto block2 = std::make_shared<go::Block>(board);

    block1->addPoint(stoneFirst->point());
    block2->addPoint(stoneSecond->point());

    board->addBlock(block1);
    board->addBlock(block2);

    auto foundBlock1 = board->findBlock(stoneFirst->point());
    auto foundBlock2 = board->findBlock(stoneSecond->point());

    // EXPECT blocks to be equal
    EXPECT_EQ(block1, foundBlock1);
    EXPECT_EQ(block2, foundBlock2);
}

TEST(Go_Stones_Test,TestCreateBoundaryForSmallBlock) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x x W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSecond = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    board->placeStone(stoneFirst);
    board->placeStone(stoneSecond);

    auto block = board->blocks().front();

    // EXPECT the block to have 1 border
    EXPECT_EQ(1, block->getBoundaries().size());

    auto boundary = *block->getBoundaries().begin();

    // EXPECT boundary to contain 6 points
    EXPECT_EQ(6, boundary->points().size());
}

TEST(Go_Stones_Test,TestCreateBoundaryForBigBlocks) {
    //      |x x x x x|
    //      |x W x W x|
    //      |x W W W x|
    //      |x x x x x|
    //      |x x x x x|
    // Blocks A and B and stone N

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneA1 = std::make_shared<go::Stone>(W, go::Point(1,1), board);
    auto stoneA2 = std::make_shared<go::Stone>(W, go::Point(1,2), board);
    auto stoneB1 = std::make_shared<go::Stone>(W, go::Point(3,1), board);
    auto stoneB2 = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneN = std::make_shared<go::Stone>(W, go::Point(2,2), board);

    board->placeStone(stoneA1);
    board->placeStone(stoneA2);
    board->placeStone(stoneB1);
    board->placeStone(stoneB2);
    board->placeStone(stoneN);

    auto block = board->blocks().front();

    // EXPECT the block to have 1 boundary
    EXPECT_EQ(1, block->getBoundaries().size());

    auto boundary = *block->getBoundaries().begin();

    // EXPECT boundary to contain 10 points
    EXPECT_EQ(10, boundary->points().size());
}


TEST(Go_Block_Test,TestCreateBoundaries) {

    //      |x x x x x|
    //      |x x b x x|
    //      |x b P b x|
    //      |x x b x x|
    //      |x x x x x|
    //
    // P = point, b = boundary points

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    go::Point point(3, 3);

    // Create a block
    go::Block block(board);

    // Add point to block
    block.addPoint(point);

    auto boundary = *block.getBoundaries().begin();

    // There should be 4 points in the boundary
    EXPECT_EQ(4, boundary->points().size());

}

TEST(Go_Block_Test,TestCreateBoundariesInCorner) {

    //      |x x x x x|
    //      |x x x x x|
    //      |x x x x x|
    //      |b x x x x|
    //      |P b x x x|
    //
    // P = point, b = boundary points

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    // In the corner
    go::Point point(0, 4);

    // Create a block
    go::Block block(board);

    // Add point to block
    block.addPoint(point);

    auto boundary = *block.getBoundaries().begin();

    // There should be 2 points in the boundary
    EXPECT_EQ(2, boundary->points().size());

}

TEST(Go_Block_Test,TestCreateBoundariesOnEdge) {

    //      |x x x x x|
    //      |b x x x x|
    //      |P b x x x|
    //      |b x x x x|
    //      |x x x x x|
    //
    // P = point, b = boundary points

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    // On edge
    go::Point point(0, 3);

    // Create a block
    go::Block block(board);

    // Add point to block
    block.addPoint(point);

    auto boundary = *block.getBoundaries().begin();

    // There should be 3 points in the boundary
    EXPECT_EQ(3, boundary->points().size());

}

TEST(Go_Stones_Test,TestisInsideBoard) {
    //      |x x x x x|
    //      |x x x x x|
    //      |W B x x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(B, go::Point(2,3), board);
    auto stoneFourth = std::make_shared<go::Stone>(W, go::Point(1,3), board);

    board->placeStone(stoneFirst);
    board->placeStone(stoneFourth);

    // EXPECT (1,3) to be a valid boundary point for black
    EXPECT_TRUE(board->isInsideBoard(go::Point(1,3)));
}

TEST(Go_Stones_Test,TestRemovePointFromBoundary) {

    go::Boundary boundary;

    boundary.addPoint(go::Point(1,2));
    boundary.addPoint(go::Point(4,2));
    boundary.addPoint(go::Point(3,2));
    boundary.addPoint(go::Point(3,3));
    boundary.addPoint(go::Point(4,4));

    int size_original = boundary.points().size();

    // Remove a point
    boundary.removePoint(go::Point(3,3));

    // EXPECT the vector to be 1 smaller
    EXPECT_EQ(size_original - 1, boundary.points().size());

}

TEST(Go_Stones_Test,TestGetBoundaryForPoint) {
    //      |x x x x x|
    //      |x x b x x|
    //      |x b W b x|
    //      |x x b x x|
    //      |x x x x x|
    // W = white stone, b = boundary points

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stone = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    board->placeStone(stone);

    auto block = board->blocks().front();

    // Find the boundary for e.g. north
    auto boundary = block->getBoundaryForPoint(go::Point(3,2));

    // EXPECT this boundary to not be nullptr
    EXPECT_TRUE(boundary != nullptr);

    // EXPECT this boundary to contain 4 points
    EXPECT_EQ(4, boundary->points().size());
}

TEST(Go_Stones_Test,TestBoundaryContains) {

    go::Point point(3,3);

    go::Boundary boundary{go::Point(1,1), go::Point(3,1), go::Point(2,2), go::Point(3,3), go::Point(4,2)};

    // EXPECT this boundary to contain 3,3
    EXPECT_TRUE(boundary.contains(point));
}

TEST(Go_Stones_Test,TestBoundaryJoin) {



    go::Point pointInBlock(2,2);
    go::Boundary boundaryToCopyFrom{go::Point(3,1), go::Point(2,2), go::Point(3,3), go::Point(4,2)};
    boundaryToCopyFrom.removePoint(pointInBlock);

    go::Point joiningPoint(3,2);
    go::Boundary boundaryToUpdate{go::Point(2,1), go::Point(1,2), go::Point(2,3), go::Point(3,2)};

    boundaryToUpdate.insertBoundaryAtPoint(boundaryToCopyFrom, joiningPoint);


    // EXPECT this boundary to be of size 6
    EXPECT_EQ(6, boundaryToUpdate.points().size());
}

TEST(Go_Stones_Test,TestBoundaryJoinWithDuplicates) {

    go::Point joiningPoint(3,2);
    go::Point pointInBlock(2,2);

    go::Boundary boundaryToUpdate{go::Point(2,1), go::Point(1,2), go::Point(2,3), go::Point(3,2), go::Point(4,2)};
    go::Boundary boundaryToCopyFrom{go::Point(3,1), go::Point(2,2), go::Point(3,3), go::Point(4,2)};

    boundaryToUpdate.insertBoundaryAtPoint(boundaryToCopyFrom, joiningPoint);

    boundaryToUpdate.removePoint(pointInBlock);

    // EXPECT this boundary to be of size 6
    EXPECT_TRUE(boundaryToUpdate.points().size());

}

TEST(Go_Stones_Test,TestBoundaryContainsAfterJoin) {

    go::Point joiningPoint(3,2);
    go::Point pointInBlock(2,2);

    go::Boundary boundaryToUpdate{go::Point(2,1), go::Point(1,2), go::Point(2,3), go::Point(3,2)};
    go::Boundary boundaryToCopyFrom{go::Point(3,1), go::Point(2,2), go::Point(3,3), go::Point(4,2)};

    boundaryToUpdate.insertBoundaryAtPoint(boundaryToCopyFrom, joiningPoint);

    boundaryToUpdate.removePoint(pointInBlock);

    // EXPECT this boundary to contain the correct elements
    EXPECT_TRUE(boundaryToUpdate.contains((go::Point(2,1))));
    EXPECT_TRUE(boundaryToUpdate.contains((go::Point(1,2))));
    EXPECT_TRUE(boundaryToUpdate.contains((go::Point(2,3))));
    EXPECT_TRUE(boundaryToUpdate.contains((go::Point(3,1))));
    EXPECT_TRUE(boundaryToUpdate.contains((go::Point(3,3))));
    EXPECT_TRUE(boundaryToUpdate.contains((go::Point(4,2))));
}

TEST(Go_Stones_Test,TestIsSuicide) {
    //      |B W x x x|
    //      |W W x x x|
    //      |x x x x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(W, go::Point(0,1), board);
    auto stoneSecond = std::make_shared<go::Stone>(W, go::Point(1,1), board);
    auto stoneThird = std::make_shared<go::Stone>(W, go::Point(1,0), board);

    board->placeStone(stoneFirst);
    board->placeStone(stoneSecond);
    board->placeStone(stoneThird);

    // EXPECT the corner to be suicide for Black
    EXPECT_TRUE(board->isSuicide(go::Point(0,0), B));
}

TEST(Go_Stones_Test,TestIsSuicideWithBigBlocks) {
    //      |B S B W x|
    //      |B W B W x|
    //      |W x W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneB1 = std::make_shared<go::Stone>(B, go::Point(0,0), board);
    auto stoneB2 = std::make_shared<go::Stone>(B, go::Point(0,1), board);
    auto stoneB3 = std::make_shared<go::Stone>(B, go::Point(2,0), board);
    auto stoneB4 = std::make_shared<go::Stone>(B, go::Point(2,1), board);
    auto stoneW1 = std::make_shared<go::Stone>(W, go::Point(0,2), board);
    auto stoneW2 = std::make_shared<go::Stone>(W, go::Point(1,1), board);
    auto stoneW3 = std::make_shared<go::Stone>(W, go::Point(2,2), board);
    auto stoneW4 = std::make_shared<go::Stone>(W, go::Point(3,1), board);
    auto stoneW5 = std::make_shared<go::Stone>(W, go::Point(3,0), board);

    board->placeStone(stoneB1);
    board->placeStone(stoneB2);
    board->placeStone(stoneB3);
    board->placeStone(stoneB4);
    board->placeStone(stoneW1);
    board->placeStone(stoneW2);
    board->placeStone(stoneW3);
    board->placeStone(stoneW4);
    board->placeStone(stoneW5);

    // EXPECT point S (0,1) to be suicide for Black
    EXPECT_TRUE(board->isSuicide(go::Point(0,1), B));
}

TEST(Go_Stones_Test,TestNotSuicideWithBigBlocks) {
    //      |B S B W x|
    //      |B W B W x|
    //      |x x W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneB1 = std::make_shared<go::Stone>(B, go::Point(0,0), board);
    auto stoneB2 = std::make_shared<go::Stone>(B, go::Point(0,1), board);
    auto stoneB3 = std::make_shared<go::Stone>(B, go::Point(2,0), board);
    auto stoneB4 = std::make_shared<go::Stone>(B, go::Point(2,1), board);
    auto stoneW1 = std::make_shared<go::Stone>(W, go::Point(1,1), board);
    auto stoneW2 = std::make_shared<go::Stone>(W, go::Point(2,2), board);
    auto stoneW3 = std::make_shared<go::Stone>(W, go::Point(3,1), board);
    auto stoneW4 = std::make_shared<go::Stone>(W, go::Point(3,0), board);

    board->placeStone(stoneB1);
    board->placeStone(stoneB2);
    board->placeStone(stoneB3);
    board->placeStone(stoneB4);
    board->placeStone(stoneW1);
    board->placeStone(stoneW2);
    board->placeStone(stoneW3);
    board->placeStone(stoneW4);

    // EXPECT point S (0,1) to NOT be suicide for Black
    EXPECT_FALSE(board->isSuicide(go::Point(0,1), B));
}

TEST(Go_Stones_Test,TestCountFreedoms) {
    //      |x x x x x|
    //      |x x x x x|
    //      |x x B x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stone = std::make_shared<go::Stone>(B, go::Point(3,3), board);

    board->placeStone(stone);

    auto block = board->findBlock(stone->point());

    // EXPECT the block to have 4 freedoms
    EXPECT_EQ(4, block->countFreedoms());
}

TEST(Go_Stones_Test,TestCountFreedomsWithOccupiedSpot) {
    //      |x x x x x|
    //      |x x x x x|
    //      |x x B W x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(2,2), board);
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(2,3), board);

    board->placeStone(stoneBlack);
    board->placeStone(stoneWhite);

    auto blockBlack = board->findBlock(stoneBlack->point());
    auto blockWhite = board->findBlock(stoneWhite->point());

    // EXPECT the black block to have 3 freedoms
    EXPECT_EQ(3, blockBlack->countFreedoms());

    // EXPECT the white block to have 3 freedoms
    EXPECT_EQ(3, blockWhite->countFreedoms());
}

TEST(Go_Stones_Test,TestIsCapture) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x W B W x|
    //      |x x W x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneW1 = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneW2 = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneW3 = std::make_shared<go::Stone>(W, go::Point(3,4), board);
    auto stoneW4 = std::make_shared<go::Stone>(W, go::Point(4,3), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(3,3), board);

    board->placeStone(stoneW1);
    board->placeStone(stoneW2);
    board->placeStone(stoneW3);
    board->placeStone(stoneBlack);

    // EXPECT White to capture a Black stone if next move is (4,3)
    EXPECT_TRUE(board->attemptCapture(go::Point(4,3), W));
}

TEST(Go_Stones_Test,TestIsCaptureAndRemove) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x W B W x|
    //      |x x W x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneW1 = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneW2 = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneW3 = std::make_shared<go::Stone>(W, go::Point(3,4), board);
    auto stoneW4 = std::make_shared<go::Stone>(W, go::Point(4,3), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(3,3), board);

    board->placeStone(stoneW1);
    board->placeStone(stoneW2);
    board->placeStone(stoneW3);
    board->placeStone(stoneBlack);

    // EXPECT White to capture a Black stone if next move is (4,3)
    EXPECT_TRUE(board->attemptCapture(go::Point(4,3), W));

    board->placeStone(stoneW4);

    // EXPECT the board to have 4 blocks after capture
    EXPECT_EQ(4, board->blocks().size());
}

TEST(Go_Stones_Test,TestCaptureScoreWithBigBlock) {
    //      |x x W x x|
    //      |x W B W x|
    //      |x W B W x|
    //      |x x W x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;
    go::StoneColor B = go::StoneColor::Black;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneW1 = std::make_shared<go::Stone>(W, go::Point(1,3), board);
    auto stoneW2 = std::make_shared<go::Stone>(W, go::Point(2,2), board);
    auto stoneW3 = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneW4 = std::make_shared<go::Stone>(W, go::Point(3,4), board);
    auto stoneW5 = std::make_shared<go::Stone>(W, go::Point(4,3), board);
    auto stoneW6 = std::make_shared<go::Stone>(W, go::Point(4,2), board);
    auto stoneB1 = std::make_shared<go::Stone>(B, go::Point(3,2), board);
    auto stoneB2 = std::make_shared<go::Stone>(B, go::Point(3,3), board);

    board->placeStone(stoneB1);
    board->placeStone(stoneB2);

    board->placeStone(stoneW1);
    board->placeStone(stoneW2);
    board->placeStone(stoneW3);
    board->placeStone(stoneW4);
    board->placeStone(stoneW5);
    board->placeStone(stoneW6);

    board->attemptCapture(stoneW6->point(), stoneW6->color());

    // EXPECT White to have 2 points
    EXPECT_EQ(2, board->whiteScore());
}

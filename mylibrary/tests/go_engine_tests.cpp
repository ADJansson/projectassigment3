
// My Go Engine
#include "../../mylibrary/go.h"
#include "../../mylibrary/stone.h"

// gtest
#include <gtest/gtest.h>



namespace go = mylib::go;


TEST(GoEngineTest_Board,FromStateConstructor) {

//      |x x x x x|
//      |x x x x x|
//      |x x b x x|
//      |x x x x x|
//      |x x x x x|

  go::Board::BoardData board_data;
  auto stone = std::make_shared<go::Stone>(go::StoneColor::Black, go::Point(3,3), nullptr);
  board_data[go::Point(3,3)] = stone;

  // Create an engine from state
  go::Board board {std::move(board_data),go::StoneColor::White,false};


  // EXPECT a stone at (3,3)
  EXPECT_TRUE(board.hasStone({3,3}));

  // EXPECT a black stone at (3,3)
  EXPECT_TRUE(board.stone({3,3}).color() == go::StoneColor::Black);
}

TEST(GoEngineTest_PreventSuicide,DiamondOfFour) {
    //      |x x x x x|
    //      |x x w x x|
    //      |x w x w x|
    //      |x x W x x|
    //      |x x x x x|

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Set up a "diamond" of whites
    go::Board::BoardData b;

      b[{3,2}] = std::make_shared<go::Stone>(W, go::Point(3,2), nullptr);
      b[{2,3}] = std::make_shared<go::Stone>(W, go::Point(2,3), nullptr);
      b[{3,4}] = std::make_shared<go::Stone>(W, go::Point(3,4), nullptr);
      b[{4,3}] = std::make_shared<go::Stone>(W, go::Point(4,3), nullptr);

      // Create a PvP engine, Black's turn
      auto engine = std::make_shared<go::Engine>();
      engine->newGameFromState( std::move(b), B, false );

      // Attempt suicide for black
      engine->placeStone(go::Point(3,3));

      // This is an  invalid move and there should not be a stone here
      EXPECT_FALSE(engine->board()->hasStone(go::Point(3,3)));
}

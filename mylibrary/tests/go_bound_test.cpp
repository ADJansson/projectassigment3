// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>
#include "../../mylibrary/stone.h"


namespace go = mylib::go;

TEST(Go_Stones_Test,TestJoinBlocks_eh) {
    //      |x x x x x|
    //      |x x W x x|
    //      |x W W x x|
    //      |x x x x x|
    //      |x x x x x|

    go::StoneColor W = go::StoneColor::White;

    // Create a board
    auto board = std::make_shared<go::Board>(go::Size(5, 5));

    auto stoneFirst = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSecond = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneThird = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    auto block1 = std::make_shared<go::Block>(board);
    auto block2 = std::make_shared<go::Block>(board);
    auto block3 = std::make_shared<go::Block>(board);

    block1->addPoint(stoneFirst->getPoint());
    block2->addPoint(stoneSecond->getPoint());
    block3->addPoint(stoneThird->getPoint());

    board->addBlock(block1);
    board->addBlock(block2);
    board->addBlock(block3);

    //board->joinBlocks(block1, block2, stoneFirst->getPoint(), stoneSecond->getPoint());
    //board->joinBlocks(block1, block3, stoneFirst->getPoint(), stoneThird->getPoint());

    // EXPECT there should be 1 block on the board
    EXPECT_EQ(1, board->getBlocks().size());

    // EXPECT this block should contain 3 elements
    EXPECT_EQ(3, block1->getPoints().size());
}

#ifndef GO_H
#define GO_H



//stl
#include <map>
#include <memory>
#include <utility>
#include <chrono>
#include <list>

#include "stone.h"
#include "block.h"


namespace mylib {

namespace go {

class Board;
struct GameState;

enum class StoneColor;
class Block;
class Stone;

class Player;
class AiPlayer;
class HumanPlayer;

class Engine;

using time_type = std::chrono::duration<int,std::milli>;

constexpr time_type operator"" _ms (unsigned long long int milli) {
    return time_type(milli);
}

constexpr time_type operator"" _s  (unsigned long long int sec) {
    return time_type(sec*1000);
}


enum class PlayerType {
    Human = 0,
    Ai = 1
};

enum class GameMode {
    VsPlayer = 0,
    VsAi = 1,
    Ai = 2
};

using Point = std::pair<int,int>;
using Size = std::pair<int,int>;


// INVARIANTS:
//   Board fullfills; what does one need to know in order to consider a given position.
//   * all stones and their position on the board
//   * who places the next stone
//   * was last move a pass move
//   * meta: blocks and freedoms

namespace priv {


class Board_base {
public:
    using BoardData = std::map<Point, std::shared_ptr<Stone>>;

    struct Position {
        BoardData board;
        StoneColor turn;
        bool was_previous_pass;

        Position () = default;
        explicit Position ( BoardData&& data, StoneColor turn, bool was_previous_pass);

    }; // END class Position


    // Constructors
    Board_base() = default;
    explicit Board_base( Size size );
    explicit Board_base( BoardData&& data, StoneColor turn, bool was_previous_pass );


    // Board data
    Size size() const;
    bool wasPreviousPass() const;
    StoneColor turn() const;
    StoneColor currentTurn() const;

    // Board
    void resetBoard( Size size );

    // Board
    Size _size;
    Position _current;

    // Blocks
    std::list<std::shared_ptr<Block>> _blocks;
    std::list<std::shared_ptr<Block>> blocks() const;
    void addBlock(std::shared_ptr<Block> block);
    void joinBlocks(std::shared_ptr<Block> newBlock, std::shared_ptr<Block> existingBlock, Point joiningPoint, Point pointInBlock);
    std::shared_ptr<Block> findBlock(Point pointInBlock) const;
    int removeBlock(std::shared_ptr<Block> block);

    // Score
    int _scoreBlack = 0;
    int _scoreWhite = 0;
    int blackScore() const;
    int whiteScore() const;


}; // END base class Board_base



} // END "private" namespace priv


class Board : private priv::Board_base, public std::enable_shared_from_this<Board>{
public:
    // Enable types
    using Board_base::BoardData;

    using Board_base::Position;

    // Enable constructors
    using Board_base::Board_base;

    // Make visible from Board_base
    using Board_base::resetBoard;
    using Board_base::size;
    using Board_base::wasPreviousPass;
    using Board_base::turn;
    using Board_base::blocks;
    using Board_base::addBlock;
    using Board_base::joinBlocks;
    using Board_base::findBlock;
    using Board_base::removeBlock;
    using Board_base::currentTurn;
    using Board_base::blackScore;
    using Board_base::whiteScore;


    // Validate
    bool isNextPositionValid( Point intersection );

    // Actions
    void placeStone( std::shared_ptr<Stone> stone );
    void passTurn();

    // Stones and positions
    bool hasStone( Point intersection ) const;
    Stone stone( Point intersection ) const;

    // Boundaries
    bool isInsideBoard(Point point) const;
    bool attemptCapture(Point point, StoneColor turn);
    bool isSuicide(Point point, StoneColor turn) const;


}; // END class Board



class Engine : public std::enable_shared_from_this<Engine> {
public:
    Engine();

    void newGame( Size size );
    void newGameVsAi( Size size );
    void newGameAiVsAi( Size size );
    void newGameFromState( Board::BoardData&& board, StoneColor turn,
                           bool was_previous_pass );

    const std::shared_ptr<Board> board() const;

    StoneColor turn() const;

    const GameMode& gameMode() const;
    const std::shared_ptr<Player> currentPlayer() const;

    void placeStone( Point intersection );
    bool validateStone( Point intersection ) const;
    void passTurn();

    void nextTurn( time_type think_time = 100_ms );
    bool isGameActive() const;


private:
    std::shared_ptr<Board> _board;

    GameMode _game_mode;
    bool _active_game;

    std::shared_ptr<Player> _white_player;
    std::shared_ptr<Player> _black_player;

}; // END class Engine


































} // END namespace go

} // END namespace mylib

#endif //GO_H

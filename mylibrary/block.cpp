#include "block.h"
#include "go.h"
#include "stone.h"
#include <algorithm>

namespace mylib {
    namespace go {


        Block::Block(std::shared_ptr<Board> board)
        {
            Block::_board = board;
        }

        Block::Block(std::initializer_list<Point> points)
        {
            for (Point point : points) {
                _points.push_back(point);
            }
        }

        // Creates a new block with boundaries
        void Block::addPoint(Point point)
        {
            _points.push_back(point);
            std::shared_ptr<Boundary> newboundary{std::make_shared<Boundary>()};

            // Insert north
            if (_board->isInsideBoard(Point(point.first, point.second - 1)))
                newboundary->addPoint(Point(point.first, point.second - 1));

            // Insert west
            if (_board->isInsideBoard(Point(point.first - 1, point.second)))
                newboundary->addPoint(Point(point.first - 1, point.second));

            // Insert south
            if (_board->isInsideBoard(Point(point.first, point.second + 1)))
                newboundary->addPoint(Point(point.first, point.second + 1));

            // Insert east
            if (_board->isInsideBoard(Point(point.first + 1, point.second)))
                newboundary->addPoint(Point(point.first + 1, point.second));

            _boundaries.push_back(newboundary);

        }

        void Block::joinBlock(std::shared_ptr<Block> oldBlock, Point joiningPoint, Point pointInBlock)
        {

            for (Point point : oldBlock->points()) {
                // Add all points
                _points.push_back(point);
            }

            std::shared_ptr<Boundary> boundaryToModify = oldBlock->getBoundaryForPoint(joiningPoint);
            std::shared_ptr<Boundary> boundaryToCopyFrom = getBoundaryForPoint(pointInBlock);

            // Remove the old boundary
            _boundaries.erase((std::find(_boundaries.begin(), _boundaries.end(), getBoundaryForPoint(pointInBlock))));

            // Copy over ALL OTHER boundaries
            for (std::shared_ptr<Boundary> boundary : oldBlock->getBoundaries()) {
                if(boundary !=boundaryToModify) {
                    _boundaries.push_back(boundary);
                }
            }

            if (boundaryToCopyFrom)
                boundaryToCopyFrom->removePoint(pointInBlock);
            if (boundaryToModify)
                boundaryToModify->insertBoundaryAtPoint(*boundaryToCopyFrom, joiningPoint);

            _boundaries.push_back(boundaryToModify);

        }

        std::list<std::shared_ptr<Boundary>> Block::getBoundaries() const
        {
            return _boundaries;
        }

        std::shared_ptr<Boundary> Block::getBoundaryForPoint(Point knownPoint) const
        {
            for (std::shared_ptr<Boundary> boundary : _boundaries) {
                for (Point point : boundary->points())
                    if (point == knownPoint)
                        return boundary;
            }
            return nullptr;
        }

        std::list<Point> Block::points() const
        {
            return _points;
        }

        int Block::countFreedoms() const
        {
            int freedoms = 0;
            for (auto boundary : _boundaries) {
                for (Point point : boundary->points())
                    if (!_board->hasStone(point))
                        freedoms++;
            }

            return freedoms;
        }

        StoneColor Block::color() const
        {
            // Assume all stones in the block are of the same color
            return _board->stone(*_points.begin()).color();
        }

        bool Block::contains(Point pointInBlock) const
        {
            return std::find(_points.begin(), _points.end(), pointInBlock) != _points.end();
        }

        Block::~Block()
        {
            _points.clear();
            _boundaries.clear();
            _board.reset();
        }

    }
}

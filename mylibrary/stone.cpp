#include "go.h"
#include "stone.h"

namespace mylib {
    namespace go {

        Stone::Stone(StoneColor color, Point position, std::shared_ptr<Board> board)
        {

            Stone::_color = color;
            Stone::_position = position;
            Stone::_board = board;
        }

        Point Stone::point() const
        {
            return Stone::_position;
        }

        StoneColor Stone::color() const
        {
            return Stone::_color;
        }

        bool Stone::hasNorth() const
        {
            Point northPoint(Stone::_position.first,
                             Stone::_position.second - 1);
            return _board->hasStone(northPoint);
        }

        bool Stone::hasSouth() const
        {
            Point southPoint(Stone::_position.first,
                             Stone::_position.second + 1);
            return _board->hasStone(southPoint);
        }

        bool Stone::hasEast() const
        {
            Point eastPoint(Stone::_position.first + 1,
                             Stone::_position.second);
            return _board->hasStone(eastPoint);
        }

        bool Stone::hasWest() const
        {
            Point westPoint(Stone::_position.first - 1,
                             Stone::_position.second);
            return _board->hasStone(westPoint);
        }

        Stone Stone::north() const
        {
            Point northPoint(Stone::_position.first,
                             Stone::_position.second - 1);
            return _board->stone(northPoint);
        }

        Stone Stone::south() const
        {
            Point southPoint(Stone::_position.first,
                             Stone::_position.second + 1);
            return _board->stone(southPoint);
        }

        Stone Stone::east() const
        {
            Point eastPoint(Stone::_position.first + 1,
                             Stone::_position.second);
            return _board->stone(eastPoint);
        }

        Stone Stone::west() const
        {
            Point westPoint(Stone::_position.first - 1,
                             Stone::_position.second);
            return _board->stone(westPoint);
        }

        Stone::~Stone()
        {
            Stone::_board.reset();
        }
    }
}

#ifndef BOUNDARY_H
#define BOUNDARY_H

#include "go.h"
#include <vector>

namespace mylib {
    namespace go {

        class Boundary {
        private:
            std::vector<Point> _points;
        public:
            Boundary();
            Boundary (std::initializer_list<Point> points);
            void addPoint(Point boundaryPoint);
            const std::vector<Point>& points() const;
            void removePoint(Point point);
            void insertBoundaryAtPoint(const Boundary& boundary, Point point);
            bool contains(Point point) const;
            bool hasAdjacent(Point point) const;
            ~Boundary();

        };
    }
}

#endif // BOUNDARY_H


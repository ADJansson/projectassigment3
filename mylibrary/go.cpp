#include "go.h"
#include "randomai.h"
#include "stone.h"
#include "block.h"
#include "boundary.h"
#include <algorithm>


namespace mylib {
namespace go {



namespace priv {

    Board_base::Board_base( Size size )
    {

        resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
        : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
        // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
        : board{data}, turn{trn}, was_previous_pass{prev_pass} {}


    void Board_base::resetBoard(Size size)
    {

        _current.board.clear();
        _size = size;
        _current.turn = StoneColor::White;
        _blocks.clear();
        _scoreBlack = 0;
        _scoreWhite = 0;

    }

    Size Board_base::size() const
    {

        return _size;
    }

    bool Board_base::wasPreviousPass() const
    {

        return _current.was_previous_pass;
    }

    StoneColor Board_base::turn() const
    {

        return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
    }

    std::list<std::shared_ptr<Block>> Board_base::blocks() const
    {
        return _blocks;
    }

    void Board_base::addBlock(std::shared_ptr<Block> block)
    {
        _blocks.push_back(block);
    }

    void Board_base::joinBlocks(std::shared_ptr<Block> firstBlock, std::shared_ptr<Block> secondBlock, Point joiningPoint, Point pointInBlock)
    {

        if (firstBlock == secondBlock)
            return;

        firstBlock->joinBlock(secondBlock, joiningPoint, pointInBlock);

        _blocks.remove(secondBlock);

    }

    std::shared_ptr<Block> Board_base::findBlock(Point pointInBlock) const
    {
        for (std::shared_ptr<Block> block: _blocks) {
            if (block->contains(pointInBlock))
                    return block;
        }
        return nullptr;
    }

    int Board_base::removeBlock(std::shared_ptr<Block> block)
    {
        int score = block->points().size();
        for (Point point : block->points()) {
            _current.board.erase(point);
        }
        _blocks.erase(std::find(_blocks.begin(), _blocks.end(), block));

        return score;
    }

    StoneColor Board_base::currentTurn() const
    {
        return _current.turn;
    }

    int Board_base::blackScore() const
    {
        return _scoreBlack;
    }

    int Board_base::whiteScore() const
    {
        return _scoreWhite;

    }
} // END namespace priv


// Board functions
void Board::placeStone(std::shared_ptr<Stone> stone)
{


    // Make a new block
    auto block = std::make_shared<Block>(shared_from_this());
    block->addPoint(stone->point());
    addBlock(block);


    // Check if there's a block to the north and add it
    if (stone->hasNorth()) {
        if (stone->north().color() == stone->color())
            joinBlocks(block, findBlock(stone->north().point()),
                       stone->point(), stone->north().point());
    }

    // Check if there's a block to the west and add it
    if (stone->hasWest()) {
        if (stone->west().color() == stone->color())
            joinBlocks(block, findBlock(stone->west().point()),
                       stone->point(), stone->west().point());
    }

    // Check if there's a block to the south and add it
    if (stone->hasSouth()) {
        if (stone->south().color() == stone->color())
            joinBlocks(block, findBlock(stone->south().point()),
                       stone->point(), stone->south().point());
    }

    // Check if there's a block to the east and add it
    if (stone->hasEast()) {
        if (stone->east().color() == stone->color())
            joinBlocks(block, findBlock(stone->east().point()),
                       stone->point(), stone->east().point());
    }

    _current.board[stone->point()] = stone;
    _current.turn = turn();

}

void Board::passTurn()
{

    _current.turn = turn();
}

bool Board::hasStone(Point intersection) const
{

    try
      {
        return _current.board.count(intersection);
      }
    catch(std::exception e) {
        return false;
    }
    return false;
}

Stone Board::stone(Point intersection) const
{

    return *_current.board.at(intersection);
}


bool Board::isNextPositionValid(Point intersection)
{

    if ((intersection.first >= 0 && intersection.first <= _size.first)
            && (intersection.second >= 0 && intersection.second <= _size.second)) {

        if(hasStone(intersection))
            return false;

        if (isSuicide(intersection, turn())) {
            if (attemptCapture(intersection, turn()))
                return true;
            else
                return false;
        }
        attemptCapture(intersection, turn());

    }
    else
        return false;

    return true;
}

bool Board::isInsideBoard(Point point) const
{
    if ((point.first >= 0 && point.first < _size.first)
                                      && (point.second >= 0 && point.second < _size.second)) {
            return true;

    }

    return false;
}

bool Board::attemptCapture(Point point, StoneColor turn)
{
    // Check for blocks in all four directions
    std::list<Point> pointsToCheck{Point(point.first, point.second - 1), Point(point.first - 1, point.second),
                Point(point.first, point.second + 1), Point(point.first + 1, point.second)};

    int capturedBlocks = 0;

    for (Point p : pointsToCheck) {
        auto block = findBlock(p);
        if (block) {
            if (block->color() != turn) {
                if (block->countFreedoms() <= 1) {
                    capturedBlocks++;
                    if (turn == StoneColor::Black)
                        _scoreBlack+= removeBlock(block);
                    else
                        _scoreWhite+= removeBlock(block);
                }

            }
        }
    }

    return capturedBlocks > 0;

}

bool Board::isSuicide(Point point, StoneColor turn) const
{
    // Check for blocks in all four directions
    std::list<Point> pointsToCheck{Point(point.first, point.second - 1), Point(point.first - 1, point.second),
                Point(point.first, point.second + 1), Point(point.first + 1, point.second)};

    for (Point p : pointsToCheck) {
        auto block = findBlock(p);
        if (block) {
            if (block->color() == turn) {
                if (block->countFreedoms() > 1) {
                    return false;
                }

            }
        }
    }

    int freedomsForPoint = 0;

    // Check freedoms for single spot
    for (Point p : pointsToCheck) {
        if (!hasStone(p) && isInsideBoard(p)) {
            freedomsForPoint++;
        }
    }

    return freedomsForPoint == 0;
}
// END Board functions


Engine::Engine()
    : _board{std::make_shared<Board>()}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

void Engine::newGame(Size size)
{

    _board->resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
}

void Engine::newGameVsAi(Size size)
{

    _board->resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
}

void Engine::newGameAiVsAi(Size size)
{

    _board->resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
}

void Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass)
{

    _board = std::make_shared<Board>(std::forward<Board::BoardData>(board),turn,was_previous_pass);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
}

const std::shared_ptr<Board> Engine::board() const
{

    return _board;
}

const GameMode& Engine::gameMode() const
{

    return _game_mode;
}

StoneColor Engine::turn() const
{

    return _board->turn();
}

const std::shared_ptr<Player> Engine::currentPlayer() const
{

    if( turn() == StoneColor::Black )
        return _black_player;
    else if( turn() == StoneColor::White )
        return _white_player;
    else
        return nullptr;
}

void Engine::placeStone(Point intersection)
{


    // Check out-of-bounds and if intersection is not occupied
    if (_board->isNextPositionValid(Point(intersection.first, intersection.second))) {

        auto stone = std::make_shared<Stone>(turn(), intersection, _board);

        _board->placeStone(stone);
    }

}

void Engine::passTurn()
{

    if(board()->wasPreviousPass()) {
        _active_game = false;
        return;
    }

    _board->passTurn();
}

void Engine::nextTurn(std::chrono::duration<int,std::milli> think_time)
{

    if( currentPlayer()->type() != PlayerType::Ai)
        return;

    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
        placeStone( p->nextStone() );
    else
        passTurn();
}

bool Engine::isGameActive() const
{

    return _active_game;
}

bool Engine::validateStone(Point pos) const
{

    return _board->isNextPositionValid(pos);
}


} // END namespace go
} // END namespace mylib

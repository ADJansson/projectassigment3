
#include "boundary.h"
#include "go.h"
#include <algorithm>
#include <cmath>

namespace mylib {
    namespace go {

    using OriginPoint = std::pair<float, float>;
    OriginPoint _origin;

    Boundary::Boundary()
    {

    }

    Boundary::Boundary(std::initializer_list<Point> points)
    {
        for (Point p : points) {
            _points.push_back(p);
        }
    }

    void Boundary::addPoint(Point boundaryPoint)
    {
            _points.push_back(boundaryPoint);
        }

        const std::vector<Point> &Boundary::points() const {
            return _points;
        }

        void Boundary::removePoint(Point point)
        {
            _points.erase(std::find(_points.begin(), _points.end(), point));
        }

        // Used for std::sort
        bool polarSort(Point p1, Point p2) {
            // http://gamedev.stackexchange.com/questions/13229/sorting-array-of-points-in-clockwise-order 4/10-15
            return (atan2(p1.second - _origin.second, p1.first - _origin.first)
                    > atan2(p2.second - _origin.second, p2.first - _origin.first));
        }

        void Boundary::insertBoundaryAtPoint(const Boundary& boundary, Point point)
        {

            _points.insert(std::find(_points.begin(), _points.end(), point),
                           boundary.points().cbegin(), boundary.points().cend());

            removePoint(point);

            float sumFirst = 0;
            float sumSecond = 0;

            // Compute origin:
            for (auto point : _points) {
                sumFirst += point.first;
                sumSecond += point.second;
            }

            _origin = OriginPoint(sumFirst / _points.size(), sumSecond / _points.size());

            std::sort(_points.begin(), _points.end(), polarSort);

            // Remove duplicates
            _points.resize( std::distance(_points.begin(), std::unique (_points.begin(), _points.end())));
        }

        bool Boundary::contains(Point point) const
        {
            return std::find(_points.begin(), _points.end(), point) != _points.end();
        }

        Boundary::~Boundary()
        {
            _points.clear();
        }

    }
}

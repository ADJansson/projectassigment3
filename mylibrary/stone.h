#ifndef STONE_H
#define STONE_H

#include "go.h"
#include <memory>

namespace mylib {
    namespace go {

        class Board;

        enum class StoneColor {
          White       = 0,
          Black       = 1
        };

        using Point = std::pair<int, int>;

        class Stone {
        private:
            StoneColor _color;
            Point _position;
            std::shared_ptr<mylib::go::Board> _board;
        public:
            Stone(StoneColor color, Point position, std::shared_ptr<Board> board);
            Point point() const;
            StoneColor color() const;

            bool hasNorth() const;
            bool hasSouth() const;
            bool hasEast() const;
            bool hasWest() const;

            Stone north() const;
            Stone south() const;
            Stone east() const;
            Stone west() const;
            ~Stone();

        };
    }
}

#endif // STONE_H


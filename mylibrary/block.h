#ifndef BLOCK_H
#define BLOCK_H

#include <set>
#include <memory>
#include "stone.h"
#include "boundary.h"

namespace mylib {
    namespace go {

        class Board;
        class Boundary;

        class Block {
        private:
            std::list<Point> _points;
            std::list<std::shared_ptr<Boundary>> _boundaries;
            std::shared_ptr<Board> _board;
        public:
            Block();
            Block(std::shared_ptr<Board> board);
            Block(std::initializer_list<Point> points);
            void addPoint(Point point);
            void joinBlock(std::shared_ptr<Block> oldBlock, Point joiningPoint, Point pointInBlock);
            std::list<std::shared_ptr<Boundary>> getBoundaries() const;
            std::shared_ptr<Boundary> getBoundaryForPoint(Point knownPoint) const;
            std::list<Point> points() const;
            int countFreedoms() const;
            StoneColor color() const;
            bool contains(Point pointInBlock) const;
            ~Block();
        };
    }
}


#endif // BLOCK_H

